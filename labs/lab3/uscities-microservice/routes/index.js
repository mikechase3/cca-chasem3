var express = require('express');
var router = express.Router();

/* MONGODB SKELETON */
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-chasem3:mouseutterbacklist@cca-chasem3.3cro9.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});

/* UKNONWN */
let fields = {_id: false, zips:true, city:true, state_id: true, state_name: true, county_name: true, timezone: true};

dbClient.connect(err =>{
  if (err) throw err;
  console.log("Connected to the MongoDB Cluster")
});

/* GET home page. */
router.get('/', function(req, res, next) {
  // res.render('index', { title: 'US Cities Microservice by Mike Chase' });
  res.send("US City Search Microservice buy Mike Chase." +
      "\nUsage: URL/uscities-search/:zips(\\\\d{1,5})");
});

/* GET search */
router.get('/uscities-search/:zips(\\d{1,5})', function(req, res){
  const db = dbClient.db();
  let zipRegEx = new RegExp(req.params.zips);
  const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
  cursor.toArray(function(err, results){
    console.log(results) // Output all records for debug only.
    res.send(results);
  })
});

router.get('/uscities-search/:city', function(req, res){
  const db = dbClient.db();
  let cityRegEx = new RegExp(req.params.city, 'i');
  const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);  // TODO: Fields is undefined
  cursor.toArray(function(err, results){
    console.log(results);
    res.send(results);

  });
});


//END

module.exports = router;
