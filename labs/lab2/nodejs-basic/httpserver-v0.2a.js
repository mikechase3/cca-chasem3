var http = require("http");
var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;
server.listen(port);
console.log("httpserver v0.2 is running at port: " + port);
const fs = require('fs'), url = require('url');

function httphandler(request,response){
    console.log("Get an HTTP REquest: " + request.url);
    var fullpath = url.parse(request.url, true);
    var localfile = ".." + fullpath.pathname;
    console.log("Debug: server's local filename = " + localfile);
    fs.readFile(localfile,(error,filecontent)=> {
        if (error){
            response.writeHead(404);
            return response.end(fullpath.pathname + " is not found on this server.");

        }
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(filecontent);
        return response.end();
    })
}