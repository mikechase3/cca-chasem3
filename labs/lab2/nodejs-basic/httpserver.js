var http = require("http");
var server = http.createServer(httphandler);  // A callback function.
var port = process.env.PORT || 8080;
server.listen(port);
console.log("httpserver is running at port: " + port);


function httphandler(request, response){
    console.log("Get an HTTP request: " + request.url);
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write("You have requested " + request.url);
    return response.end();
}