var express = require('express');
var router = express.Router();
const server = require('http').createServer(router);
const io = require('socket.io')(server);

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  res.sendFile(__dirname + '/static/chatclient.html');

});

// io.on('connection', (socketclient) => {
// 	console.log('A new client is connected!');
//     /* code to handle*/
// });



module.exports = router;
