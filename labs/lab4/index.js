const express= require('express');
const app=express();
var port = process.env.PORT || 8080; // important for deployment later
const server = require('http').createServer(app);
const io = require('socket.io')(server);
server.listen(port); //changed from app.listen(port)
app.use(express.static('static'));
app.use(express.urlencoded({extended: false}));
console.log("WebChat server is running on port " + port);
app.get("/", (req,res)=>{
    res.sendFile(__dirname + '/static/chatclient.html')
})
io.on('connection', (socketclient) => { 
	console.log('A new client is connected!'); 
    /* code to handle*/
    socketclient.on("message", (data) => {
        console.log('Data from a clinet: '+ data);
    });
});
